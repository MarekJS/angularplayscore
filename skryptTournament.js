        app.controller('formCtrl', function ($scope) {

            localStorage.clear();

            $scope.gameModes = ["League one leg", "League two leg", "Brackets"];
            $scope.count = 0;
            $scope.master = {
                name: "Turniej testowy",
                gameMode: "League one leg",
                matchTime: 20,
                stations: 2,
                players: [{
                        id: 'nr1',
                        name: 'playername1'
                }, {
                        id: 'nr2',
                        name: 'playername2'
                }, {
                        id: 'nr3',
                        name: 'playername3'
                }, {
                        id: 'nr4',
                        name: 'playername4'
                },
                         ]
            };

            $scope.reset = function () {
                $scope.tournament = angular.copy($scope.master);
            };
            $scope.reset();
            $scope.newPlayer = function () {
                $scope.tournament.numberPeople += 1;
            };

            $scope.addNewChoice = function () {
                var newItemNo = $scope.tournament.players.length + 1;
                $scope.tournament.players.push({
                    'id': 'nr' + newItemNo
                });
            };

            $scope.removeChoice = function () {
                var lastItem = $scope.tournament.players.length - 1;
                $scope.tournament.players.splice(lastItem);
            };

            $scope.saveTournamentData = function () {
                //localStorage.setItem("tournamentData", JSON.stringify($scope.tournament));
                localStorage.setItem("playersData", JSON.stringify($scope.tournament.players));
            }


        });