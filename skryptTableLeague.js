app.controller("tableLeagueCtrl", function ($scope) {

    var playersData = JSON.parse(localStorage.getItem("playersData"));
    $scope.tablePlayers = [];
    $scope.state1 = false;
    $scope.state2 = true;
    $scope.state3 = true;
    var min = 0;
    $scope.tableSortOrder = "-points";

    var createPlayers = function () {
        for (var i = 0; i < playersData.length; i++) {
            var tempPlayer = {};
            tempPlayer.id = playersData[i].id;
            tempPlayer.name = playersData[i].name;
            tempPlayer.matches = 0;
            tempPlayer.points = 0;
            tempPlayer.goals = 0;
            tempPlayer.lostGoals = 0;
            tempPlayer.win = 0;
            tempPlayer.draw = 0;
            tempPlayer.lose = 0;

            $scope.tablePlayers.push(tempPlayer);
        }
    };

    var scheduleData = JSON.parse(localStorage.getItem("scheduleData"));
    if (scheduleData === null) {
        console.log("nie ma danych w local storage");
        createPlayers();
    } else {
        $scope.tablePlayers = scheduleData;
    }

    var sortTable = function () {
        var newTable = [];
        for (var i = 0; i < playersData.length; i++) {
            if ($scope.tablePlayers[i].matches == min) {
                newTable.push($scope.tablePlayers[i])
            }
        }
        return newTable;
    }

    var myFun = function (tab) {
        var item1 = tab[Math.floor(Math.random() * tab.length)].id;
        console.log(item1);
        var item2 = tab[Math.floor(Math.random() * tab.length)].id;
        console.log(item2);
        if (item1 != item2) {
            $scope.drawnPlayer1 = item1;
            $scope.drawnPlayer2 = item2;
        } else {
            myFun(tab);
        }
    }


    $scope.nextMatch = function () {
        var cTable = sortTable();
        myFun(cTable);
    }

    $scope.score1 = 0;
    $scope.score2 = 0;

    var minMatch = function () {
        var min = $scope.tablePlayers[0].matches;
        for (var i = 0; i < playersData.length; i++) {
            if ($scope.tablePlayers[i].matches < min) {
                min = $scope.tablePlayers[i].matches;
            }
        }
        return min;
    }

    $scope.saveScore = function () {
        var score1 = $scope.score1;
        var score2 = $scope.score2;

        for (var i = 0; i < playersData.length; i++) {
            if ($scope.tablePlayers[i].id === $scope.drawnPlayer1) {
                $scope.tablePlayers[i].matches += 1;
                $scope.tablePlayers[i].goals += score1;
                $scope.tablePlayers[i].lostGoals += score2;

                if (score1 === score2) {
                    $scope.tablePlayers[i].draw += 1;
                    $scope.tablePlayers[i].points += 1;
                } else if (score1 > score2) {
                    $scope.tablePlayers[i].win += 1;
                    $scope.tablePlayers[i].points += 3;
                } else if (score1 < score2) {
                    $scope.tablePlayers[i].lose += 1;
                }
            } else if ($scope.tablePlayers[i].id === $scope.drawnPlayer2) {
                $scope.tablePlayers[i].matches += 1;
                $scope.tablePlayers[i].goals += score2;
                $scope.tablePlayers[i].lostGoals += score1;

                if (score1 === score2) {
                    $scope.tablePlayers[i].draw += 1;
                    $scope.tablePlayers[i].points += 1;
                } else if (score1 < score2) {
                    $scope.tablePlayers[i].win += 1;
                    $scope.tablePlayers[i].points += 3;
                } else if (score1 > score2) {
                    $scope.tablePlayers[i].lose += 1;
                }
            }
        }

        min = minMatch();
        //save to local storage
        console.log("before:" + $scope.tablePlayers);
        localStorage.setItem("scheduleData", JSON.stringify($scope.tablePlayers));
        console.log("after:" + JSON.stringify($scope.tablePlayers));
    }



});